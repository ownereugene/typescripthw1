import { v4 as uuidv4 } from 'uuid';
enum CurrencyEnum {
  USD = 'USD',
  UAH = 'UAH',
}
class Transaction {
  public readonly id: string;
  public readonly amount: number;
  public readonly currency: CurrencyEnum;

  constructor(amount: number, currency: CurrencyEnum) {
    this.id = uuidv4(); 
    this.amount = amount;
    this.currency = currency;
  }
}
class Card {
  private transactions: Transaction[] = [];
  public AddTransaction(transaction: Transaction): string {
    this.transactions.push(transaction);
    return transaction.id;
  }
  public AddTransactionWithUSD(currency: CurrencyEnum, amount: number): string {
    const transaction = new Transaction(amount, currency);
    this.transactions.push(transaction);
    return transaction.id;
  }

  public GetTransaction(id: string): Transaction | undefined {
    return this.transactions.find((transaction) => transaction.id === id);
  }

  public GetBalance(currency: CurrencyEnum): number {
    return this.transactions
      .filter((transaction) => transaction.currency === currency)
      .reduce((total, transaction) => total + transaction.amount, 0);
  }
}

const card = new Card();

const transactionId1 = card.AddTransactionWithUSD(CurrencyEnum.USD, 100);
const transactionId2 = card.AddTransactionWithUSD(CurrencyEnum.UAH, 200);
const transactionId3 = card.AddTransaction(new Transaction(50, CurrencyEnum.USD));

console.log(card.GetTransaction(transactionId1));
console.log(card.GetTransaction(transactionId2));
console.log(card.GetTransaction(transactionId3));

console.log('Balance in USD:', card.GetBalance(CurrencyEnum.USD));
console.log('Balance in UAH:', card.GetBalance(CurrencyEnum.UAH));
